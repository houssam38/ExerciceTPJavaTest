package fr.houssam;

public class FizzBuzz {

	public String algo(int i)
	{
		//return i % 15 == 0 ? "FIZZBUZZ" : i % 3 == 0 ? "FIZZ" : i % 5 == 0 ? "BUZZ" : String.valueOf(i); 
		if((i % 3) == 0 && (i % 5) == 0){
			return "FIZZBUZZ";
		}
		if(i % 3 == 0) {
			return "FIZZ";
		}
		if(i % 5 == 0) {
			return "BUZZ";
		}
		if(i % 15 == 0)
		{
			return "FIZZBUZZ";
		}
		return String.valueOf(i);
	}

}
