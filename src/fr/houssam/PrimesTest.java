package fr.houssam;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;



public class PrimesTest {

	@Test
	public void forNumberBelow2IsAnEmptyist() {
		assertEquals(Collections.emptyList(), this.factors(0));
		assertEquals(Collections.emptyList(), this.factors(1));
	}
	@Test
	public void for2ItIsAsListof2()
	{
		assertEquals(Arrays.asList(2), this.factors(2));
	}
	
	
	private ArrayList<Integer> factors(int i) 
	{
		Primes primes = new Primes();
		ArrayList<Integer> factors = primes.factorsOf(i);
		return factors;
	}


}
