/**
 * 
 */
package fr.houssam;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author houssam
 *
 */
public class FizzBuzzTest {

	@Test
	public void testforNumbers() {
		FizzBuzz buzz = new FizzBuzz();
		
		//for(int i=1, i<=)
		
		assertEquals("1", buzz.algo(1));
		assertEquals("2", buzz.algo(2));
		assertEquals("4", buzz.algo(4));
		assertEquals("7", buzz.algo(7));
	}
	@Test
	public void testMultipleDe3()
	{
		FizzBuzz buzz = new FizzBuzz();
		
		assertEquals("FIZZ", buzz.algo(3));
		assertEquals("FIZZ", buzz.algo(6));
		assertEquals("FIZZ", buzz.algo(9));
		assertEquals("FIZZBUZZ", buzz.algo(15));
	}
	
	@Test
	public void testMultiplede7()
	{
		FizzBuzz buzz = new FizzBuzz();
		assertEquals("BANG", buzz.algo(7));
	}
	
	@Test
	public void testMultipleDe5()
	{
		FizzBuzz buzz = new FizzBuzz();
		
		assertEquals("BUZZ", buzz.algo(5));
		assertEquals("BUZZ", buzz.algo(10));
		assertEquals("FIZZBUZZ", buzz.algo(15));
	}
}
